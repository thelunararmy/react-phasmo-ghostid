export function msToMinSecs(millis) {
  const date = new Date(millis);
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  return `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;
}
