# `react-phasmo-ghostid`
A React web app to track ghostly activity in the popular Steam game `Phasmophobia`.

![](git_assets/example_signs.png)

## Background
[Phasmophobia](https://store.steampowered.com/app/739630/Phasmophobia/) is a 4 player coop game, built in Unity3D, where ghost hunters are tasked to identify spooky entities within various in-game locations. To assist them, they are given ghost hunting equipment which will give a positive reading if the particular ghost they are hunting reacts to the device. When a positive reading is recorded the players have to log their finding in their in-game journal. This is a very annoying task to do because each ghost can only be a combination of three different signs. Once the player starts filling in their journal the number of possible combinations narrows... but the journal itself currently has no "at a glace" way of showing the player what clues to look for next. This is what this app aims to do.

## This project
This projects aim to a provide an "at a glance" app that can easily be accessed via webbrowser on desktop or mobile. A live version of the app can be accessed here:

- https://thelunararmy.gitlab.io/react-phasmo-ghostid/

## How to use
Simply tap on the icons representing each of the in game ghost signs as you discover them. Each ghost only has three known signs. Once you select two signs the remaining sign will quickly reduce to one or two possibilies. The remaining elusive sign can be seen by an active icon that can be clicked. Once all three signs are selected, the offending ghost type will be displayed. To unselect a sign, simply click on the icon again.

A table of each sign with corresponding icon can be seen below. 

| Sign          | Symbol                              | Tool used    |
|:--------------|:-----------------------------------:|-------------:|
|EMF Level Five |![](git_assets/icon_emf5.png)        | EMF Reader   |
|Fingerprints   |![](git_assets/icon_fingerprint.png) | UV Light     |
|Freezing Temp  |![](git_assets/icon_freezing.png)    | Thermometer  |
|Ghost Writing  |![](git_assets/icon_menubook.png)    | Book         |
|Spirit Box     |![](git_assets/icon_radio.png)       | Spirit Box   |
|Ghost Orbs     |![](git_assets/icon_ghost.png)       | Video Camera |

## Running this app on your local machine
1. Clone this repo
2. Navigate to the folder via terminal
3. Type `npm run`
4. Local dev server deploys to `localhost:3000`. Access it via webbrowser.

## Acknowledgement
I got into the development thanks to my buddy JackGrimm who made a table app for us to use in our games. I did not my make app to compete with his, but to push my own abilities as a UX designer. Check out his original version [here](https://jackgrimm.pythonanywhere.com/phasmo). The concept belongs to him.

## Future plans
- ~~Refactor table so it doesn't crop on mobile~~ ✔️ added
- ~~Replace freezing temperature icon with something less numeric.~~ ✔️ added
- ~~More information about the ghost once you've figured it out.~~ ✔️ added
- ~~Dark mode~~ ✔️ added
- ~~Reset button~~ ✔️ added
- ~~General house keeping: better url name, better web page meta data~~ ✔️ added
- ????

## Contributing
If you have any sugguestions then feel free to open an issue.

## Contributors
- @FukkenHanzel - 🇪🇸 Spanish translation
